// ==UserScript==
// @name         ZD Table Shortcut
// @namespace    https://gitlab.com/rverschoor/punk
// @version      0.1
// @description  Adds a userscript menu button to set the clipboard to a table HTML element
// @author       @rrelax
// @license     MIT
// @match        https://gitlab.zendesk.com/agent/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=zendesk.com
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_table_shortcut
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_table_shortcut/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_table_shortcut/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_table_shortcut/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// @grant    GM.getValue
// @grant    GM.setValue
// @grant    GM.registerMenuCommand
// ==/UserScript==

(function() {
    'use strict';

    function isNullOrUndefined(item) {
        return item == undefined || item == null;
    }

    async function getState(stateVariableName, defaultValue) {
        console.log(`Getting state: '${stateVariableName}'`);
        let variableValue = await GM.getValue(stateVariableName);

        if(!isNullOrUndefined(variableValue)) {
            console.log(`'${stateVariableName}': ${variableValue}`);
            return variableValue;
        } else {
            console.log(`'${stateVariableName}': ${defaultValue}`);
            return defaultValue;
        }
    }

    function addUserMenuScript() {
        GM.registerMenuCommand("Set table clipboard", async () => setClipboard('text/html',
            await createTableElement()
       ));
    }

    async function createTableElement() {
        let element = document.createElement("table");
        element.style.backgroundColor = await getState("emptyTableBackgroundColor","#f5f4f2");
        element.style.borderCollapse = "collapse";

        const rowGenerator = (tagName, innerHtmlList) => {
            let rowElement = document.createElement("tr");

            innerHtmlList.map(item => {
                let newElement = document.createElement(tagName);
                newElement.innerHTML = item;
                newElement.style.border = `1px solid ${getState("emptyTableBorderColor")}`;
                rowElement.appendChild(newElement);
            })
            return rowElement;
        }

        element.appendChild(rowGenerator("th", ["1", "2"]))
        element.appendChild(rowGenerator("td", [" ", " "]))

        return element.outerHTML;
    }

    async function setClipboard(contentType, content) {
        const type = contentType // [ "text/html", "text/plain"];
        const blob = new Blob([content], { type });
        const data = [new ClipboardItem({ [type]: blob })];

        await navigator.clipboard.write(data);
    }

    addUserMenuScript();
})();