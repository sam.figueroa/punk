// ==UserScript==
// @name          GitLab hide MR bot comments
// @version       1.2
// @author        Rene Verschoor
// @description   GitLab: hide MR bot comments
// @match         https://gitlab.com/gitlab-org/gitlab/-/merge_requests/*
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/gitlab_hide_mr_bot_comments
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/gitlab_hide_mr_bot_comments/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/gitlab_hide_mr_bot_comments/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/gitlab_hide_mr_bot_comments/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

const BOTS = [
  'Danger bot',
  'CI scripts API usage',
  'gitlab-org/database-team/gitlab-com-database-testing',
  'TEST_SLOW_NOTE_PROJECT_TOKEN',
  'DANGER_GITLAB_API_TOKEN',
];

let visible = true;

(function() {

  function toggleBotComments() {
    visible = !visible;
    const comments = document.querySelectorAll('.timeline-entry');
    comments.forEach((comment, index) => {
      const avatar = comment.querySelector('.timeline-avatar a img');
      if (avatar) {
        if (BOTS.includes(avatar.alt)) {
          toggeVisibility(comments, index);
        }
      }
    })
  }

  function toggeVisibility(comments, index) {
    comments[index].style.display = visible? 'block' : 'none';
  }

  function hotKeyHandler(event) {
    if(event.ctrlKey && event.altKey && event.code === "KeyB") {
      toggleBotComments();
    }
  }

  window.addEventListener("keydown", hotKeyHandler, true);

})();
