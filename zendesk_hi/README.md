# Zendesk Hi

## Purpose

Say `Hi` to the ticket requester.

Use the `Control-H-I` key combination to paste `Hi <first name of requester>,` in your reply.\
For L&R IR tickets it uses the name of the first Cc:.

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hi/script.user.js

## Changelog

- 1.4 Handle 'IR - ' title change
- 1.3 Position caret after paste
- 1.2 Focus & preserve content
- 1.1 DIY hotkey handler
- 1.0 Public release
