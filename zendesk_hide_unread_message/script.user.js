// ==UserScript==
// @name          Zendesk Hide Unread Message
// @version       1.0
// @author        Rene Verschoor
// @description   Zendesk: Hide Unread Message
// @match         https://gitlab.zendesk.com/*
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_hide_unread_message
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_unread_message/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_unread_message/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_unread_message/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

(function() {
  const observeNode = document.querySelector('body');
  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  const observer = new MutationObserver( mutations => {
    pogo(mutations);
  });
  observer.observe(observeNode, { childList: true, subtree: true });

  function pogo(mutations) {
    mutations.forEach( mutation => {
      mutation.target.querySelectorAll('button[data-test-id="omnilog-jump-button"]').forEach( element => {
        element.style.visibility = 'hidden';
      });
      mutation.target.querySelectorAll('div[data-test-id="omnitab-unread-message-count"]').forEach( element => {
        element.style.visibility = 'hidden';
      });
    })
  }

})();
