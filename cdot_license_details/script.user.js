// ==UserScript==
// @name          cdot_license_details
// @version       1.10
// @author        Rene Verschoor
// @description   CDot License details
// @match         https://customers.gitlab.com/*
// @match         https://customers.staging.gitlab.com/*
// @grant         GM_setClipboard
// @require       https://cdn.jsdelivr.net/npm/sweetalert2@11
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/cdot_license_details
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/cdot_license_details/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/cdot_license_details/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/cdot_license_details/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

(function() {
  // Watch for a page URL change
  let previousUrl = '';
  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  let observer = new MutationObserver(function (mutations, observer) {
    let url = location.href;
    if (url !== previousUrl) {
      previousUrl = url;
      po(url);
    }
  });
  observer.observe(document, { childList: true, subtree: true });

  function po(url) {
    // Only act when we're looking at a license details page
    if (url.match(/^https:\/\/customers\.(staging\.)?gitlab.com\/admin\/license\/\d+$/)) {
      go();
    }
  }

  async function go() {
    // Collect license info
    const info = await scrape();
    // Display license info My Way
    await myWay(info);
  }

  async function scrape() {
    let info = {};
    await delay(DELAY);
    hideOriginalInfo();
    const headers = document.querySelectorAll('.card-header');
    if (headers.length) {
      for (let i = 0; i < headers.length; i++) {
        // Field name, eg '  First name  \n' -> remove whitespace and LF/CR, lowercase, snake_case -> 'first_name'
        const key = headers[i].textContent.trim().toLowerCase().replace(/ /g, '_');
        const value = headers[i].nextElementSibling.textContent.trim();
        const url = headers[i].nextElementSibling.querySelector('a')?.getAttribute('href');
        info[key] = {value: value, url: url};
      }
    }
    return info;
  }

  const DELAY = 200;

  function delay(ms) {
    return new Promise(res => setTimeout(res, ms));
  }

  const css =
    `
      <style>
        #punk {
          display: flex;
          flex-wrap: wrap;
          align-items: start;
          margin-bottom: 1rem;
        }
        table {
          margin: 0.5rem;
        }
        td.label {
          text-align: right;
          padding-left: 0.5rem;
          padding-right: 0.8rem;
          background-color: rgb(236, 236, 239);
          border-left: solid white 2px;
        }
        td.value {
          padding-left: 0.2rem;
          padding-right: 0.5rem;
        }
        #toggleButton {
          padding: 0.5rem;
          margin-bottom: 1rem;
          border: 0;
        }
        #copyEmail,
        #copyZuoraName {
          cursor: pointer; 
        }
      </style>
      `

  async function myWay(info) {
    await delay(DELAY);
    const punked = await alreadyPunked();
    if (!punked) {
      await addInfoToPage(info);
      await delay(DELAY);
    }
    addToggleButtonListener();
    addEmailCopyListener(info);
    addZuoraNameCopyListener(info);
  }

  async function alreadyPunked() {
    return document.querySelector('div#punk');
  }

  async function addInfoToPage(info) {
    let html = '';
    html += '<div id="punk">'
    html += addInfoTable(info);
    html += addAnalysis(info);
    html += '</div>'
    html += addToggleButton();
    addToDOM(html);
  }

  function addToDOM(html) {
    const div = document.createElement('div')
    div.innerHTML += css;
    div.innerHTML += html;

    const target = document.querySelector('div.fieldset')
    target.appendChild(div)
    target.before(div)
  }

  function addToggleButton() {
    return '<button id="toggleButton">Show original info</button>'
  }

  function addInfoTable(info) {
    let html = '';
    infoTable(info).forEach((section) => {
      html += '<table>'
      section.forEach((line) => {
        html += tableRow(line.label, showValue(line.label, line.info));
      });
      html += '</table>'
    });
  return html;
  }

  function tableRow(label, value) {
    let html = '';
    const label_text = label === null ? '&nbsp' : label;
    const value_text = value === null ? '' : value;
    html += '<tr>';
    html += `<td class="label">${label_text}</td>`
    html += `<td class="value">${value_text}</td>`
    html += '</tr>';
    return html;
  }

  function infoTable(info) {
    return [
      [ {label: 'Company', info: info.company},
        {label: 'Name', info: info.name},
        {label: 'Email', info: info.email},
        {label: null, info: null},
        {label: 'Type', info: info.type},
        {label: 'Plan', info: info.gitlab_plan},
        {label: 'Trial', info: info.trial},
        {label: null, info: null},
        {label: 'Users count', info: info.users_count},
        {label: 'Previous users', info: info.previous_users_count},
        {label: 'Trueup count', info: info.trueup_count}
      ],
      [ {label: 'Issued at', info: info.issued_at},
        {label: 'Starts at', info: info.starts_at},
        {label: 'Expires at', info: info.expires_at},
        {label: null, info: null},
        {label: 'Zuora id', info: info.zuora_subscription},
        {label: 'Zuora name', info: info.zuora_subscription_name},
        {label: null, info: null},
        {label: 'Creator', info: info.creator},
        {label: 'Notes', info: info.notes}
      ]
    ];
  }

  function showValue(label, info) {
    const value = info === null ? '' : info?.value;
    let text;
    // Some values still have a trailing space...
    value?.trim();
    switch (label) {
      case 'Company': {
        text = showCompany(info);
        break;
      }
      case 'Name': {
        text = showName(info);
        break;
      }
      case 'Email': {
        text = showEmail(value);
        break;
      }
      case 'Zuora id': {
        text = showZuoraId(info);
        break;
      }
      case 'Zuora name': {
        text = showZuoraName(value);
        break;
      }
      case 'Notes': {
        text = showNotes(value);
        break;
      }
      default: {
        text = value;
      }
    }
    return text;
  }

  function showCompany(info) {
    return linkifyInfo(info);
  }

  function showName(info) {
    return linkifyInfo(info);
  }

  function showEmail(value) {
    return linkifyCDotLicensesEmail(value) + showEmailCopy();
  }

  function showEmailCopy() {
    return '&nbsp;<i id="copyEmail">❐</i>';
  }

  function showZuoraId(info) {
    if (info.value === 'N/A') {
      return info.value;
    } else {
      return linkifyInfo(info);
    }
  }

  function showZuoraName(value) {
    if (value === 'N/A') {
      return value;
    } else {
      return value + showZuoraNameCopy() + '&nbsp;&nbsp;&nbsp;' + showZuoraNameCloudActivations(value);
    }
  }

  function showZuoraNameCopy() {
    return '&nbsp;<i id="copyZuoraName">❐</i>';
  }

  function showZuoraNameCloudActivations(value) {
    return linkifyCDot('cloud_activation', value, '☁️')
  }

  function showNotes(value) {
    let txt = '';
    let segments = value.split(/(https:\/\/gitlab.zendesk.com\/agent\/tickets\/\d*)/);
    segments.forEach(segment => {
      txt += linkifyNote(segment);
    })
    return txt;
  }

  function showHostnames(info) {
    return linkifyValueUrl('version.gitlab.com', info.url);
  }

  function showSeatLinks(info) {
    let txt = '';
    if (info.value !== 'N/A') {
      txt = linkifyCDotSeatLinks(info.value);
    }
    return txt;
  }

  function linkifyNote(value) {
    if (value.startsWith('https://')) {
      return linkifyValueUrl(value, value);
    } else {
      return value;
    }
  }

  function linkifyInfo(info) {
    return linkifyValueUrl(info.value, info.url);
  }

  function linkifyValueUrl(value, url) {
    return `<a href="${url}">${value}</a>`;
  }

  function linkifyCDotSeatLinks(value) {
    return linkifyCDot('license_seat_link', value, value)
  }

  function linkifyCDotLicensesEmail(email) {
    return linkifyCDotEmail('license', email);
  }

  function linkifyCDotCustomersEmail(email) {
    return linkifyCDotEmail('customer', email);
  }

  function linkifyCDotEmail(path, email) {
    const parts = splitEmail(email);
    return linkifyCDot(path, email, parts.name) + '@' + linkifyCDot(path, parts.domain, parts.domain);
  }

  function linkifyCDot(path, value, text = value) {
    const base = document.location.origin;
    const url = new URL(`${base}/admin/${path}`);
    url.searchParams.append('query', `${value}`);
    return `<a href="${url}">${text}</a>`;
  }

  function splitEmail(email) {
    const parts = email.split('@');
    return { name: parts[0], domain: parts[1]};
  }

  function linkifySFDCEmail(email) {
    const parts = splitEmail(email);
    return linkifySFDC(email, parts.name) + '@' + linkifySFDC(parts.domain, parts.domain);
  }

  function linkifySFDC(value, text = value) {
    const url = new URL('https://gitlab.my.salesforce.com/_ui/search/ui/UnifiedSearchResults');
    url.searchParams.append('str', `${value}`);
    return `<a href="${url}">${text}</a>`;
  }

  function addToggleButtonListener() {
    const button = document.getElementById ("toggleButton");
    button.addEventListener ("click", toggleOriginalInfo, false);
  }

  function toggleOriginalInfo() {
    if (this.textContent.startsWith('Show')) {
      this.textContent = 'Hide original info';
      showOriginalInfo();
    } else {
      this.textContent = 'Show original info';
      hideOriginalInfo();
    }
  }

  function showOriginalInfo() {
    const originalInfo = document.getElementsByClassName("fieldset");
    originalInfo[0].style.visibility = 'visible';
  }

  function hideOriginalInfo() {
    const originalInfo = document.getElementsByClassName("fieldset");
    originalInfo[0].style.visibility = 'hidden';
  }

  function addEmailCopyListener(info) {
    const icon = document.getElementById ("copyEmail");
    if (icon) {
      icon.addEventListener ("click", () => { copyEmail(info) }, false);
    }
  }

  function copyEmail(info) {
    GM_setClipboard(info.email.value, 'text/plain');
    toast_ok('Email copied');
  }

  function addZuoraNameCopyListener(info) {
    const icon = document.getElementById ("copyZuoraName");
    if (icon) {
      icon.addEventListener("click", () => {
        copyZuoraName(info)
      }, false);
    }
  }

  function copyZuoraName(info) {
    GM_setClipboard(info.zuora_subscription_name.value, 'text/plain');
    toast_ok('Zuora Name copied');
  }

  function addAnalysis(info) {
    let html = '';
    html += '<table>'
    html += analysisTrial(info);
    html += analysisType(info);
    html += analysisDuration(info);
    html += analysisExpiration(info);
    html += addLinks(info);
    html += '</table>'
    return html;
  }

  function analysisTrial(info) {
    let html = '';
    if (isTrial(info)) {
      html += tableRow('🟢', 'Trial license');
      if (info.temporary_extension.value) {
        html += tableRow('SFDC', linkifyValueUrl('history', info.temporary_extension.url));
      }
    }
    return html;
  }

  function analysisType(info) {
    const types = { 'Legacy': '⌛', 'Offline': '🔒', 'Cloud': '☁️' };
    return tableRow(types[info.type.value], info.type.value);
  }

  function analysisDuration(info) {
    let html = '';
    const trial = isTrial(info);
    let oneyear = exactlyOneYear(info.starts_at.value, info.expires_at.value);
    let yearplus = oneYearOrMore(info.starts_at.valueOf(), info.expires_at.value);
    if (!trial && !oneyear) {
      html = tableRow('📅', `License != 1y ❗`);
    }
    if (trial && yearplus) {
      html = tableRow('📅', `Trial >= 1y ❗`);
    }
    return html;
  }

  function analysisExpiration(info) {
    let html = '';
    const today = new Date();
    const expiration = new Date(info.expires_at.value);
    if (expiration <= today) {
      html = tableRow('⏰', `Expired ❗`);
    }
    return html;
  }

  function exactlyOneYear(start, end) {
    const startYMD = start.split('-');
    const endYMD = end.split('-');
    return ( (endYMD[0] - startYMD[0] === 1) && (startYMD[1] === endYMD[1]) && (startYMD[2] === endYMD[2]) );
  }

  function oneYearOrMore(start, end) {
    const DAY_UNIT_IN_MILLISECONDS = 24 * 3600 * 1000
    const diffInMilliseconds = new Date(end).getTime() - new Date(start).getTime()
    const diffInDays = diffInMilliseconds / DAY_UNIT_IN_MILLISECONDS
    return (diffInDays >= 365);
  }

  function isTrial(info) {
    return (info.trial.value === 'Yes');
  }

  // https://github.com/sweetalert2/sweetalert2
  function _toast(icon, message) {
    let timerInterval
    Swal.fire({
      icon: icon,
      title: message,
      showConfirmButton: false,
      timer: 1500,
      willClose: () => {clearInterval(timerInterval)},
    });
  }

  function toast_ok(message) {
    _toast('success', message);
  }

  function addLinks(info) {
    let html = '';
    html += tableRow('&nbsp;', '');
    html += tableRow('CDot Licenses', linkifyCDotLicensesEmail(info.email.value));
    html += tableRow('CDot Customers', linkifyCDotCustomersEmail(info.email.value));
    html += tableRow('SFDC', linkifySFDCEmail(info.email.value));
    html += tableRow('&nbsp;', '');
    html += tableRow('Hostnames', showHostnames(info.hostnames_with_this_license));
    html += tableRow('Seat links', showSeatLinks(info.zuora_subscription_name));
    return html;
  }
  })();
