// ==UserScript==
// @name          Zendesk hide internal request SLA Bot bumps
// @version       1.1
// @author        Rene Verschoor
// @description   Zendesk: internal request SLA Bot bumps
// @match         https://gitlab.zendesk.com/*
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_hide_ir_sla_bot_bumps
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_ir_sla_bot_bumps/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_ir_sla_bot_bumps/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_ir_sla_bot_bumps/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

const DEBUG = false;

/*
GitLab Support End User Friday 10:32
I am a bot replying on this ticket to get the SLA timer back in place
*/

(function() {

  // Watch for any change on the Zendesk page
  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  const observer = new MutationObserver(function (mutations, observer) {
    po();
  });
  observer.observe(document.body, { childList: true, subtree: true });

  // Find the bump comments
  function po() {
    let comments = document.querySelectorAll('[data-test-id="omni-log-comment-item"]');
    comments.forEach((comment, index) => {
      if (comment.querySelector('[data-test-id="omni-log-item-sender"]').textContent.trim() === 'GitLab Support End User') {
        let contents = comment.querySelectorAll('[data-test-id="omni-log-item-message"] .zd-comment p');
        if (contents.length == 1) {
          if (contents[0].textContent === 'I am a bot replying on this ticket to get the SLA timer back in place') {
            go(comments, index);
          }
        }
      }
    })
  }

  // Remove elements
  function go(elements, index) {
    if (DEBUG) {
      elements[index].style.setProperty('background-color', 'hotpink', 'important')
    } else {
      elements[index].style.display = 'none';
    }
  }

})();
