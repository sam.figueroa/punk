'use strict';

function scrapeSfdcSubscription(url) {
  let info = {};
  info.url = url;
  let labelNodes = document.querySelectorAll('.detailList tr td.labelCol');
  info.subscription_name = labelToValue('Subscription Name', labelNodes);
  info.version = labelToValue('Version', labelNodes)
  info.account = labelToValue('Account', labelNodes);
  info.billing_account = labelToValue('Billing Account', labelNodes);
  info.invoice_owner = labelToValue('Invoice Owner', labelNodes);
  info.status = labelToValue('Status', labelNodes);
  info.initial_term = labelToValue('Initial Term', labelNodes);
  info.renewal_term = labelToValue('Renewal Term', labelNodes);
  info.current_term = labelToValue('Current Term', labelNodes);
  info.term_start_date = labelToValue('Term Start Date', labelNodes);
  info.term_end_date = labelToValue('Term End Date', labelNodes);
  info.subscription_start_date = labelToValue('Subscription Start Date', labelNodes);
  info.subscription_end_date = labelToValue('Subscription End Date', labelNodes);
  info.next_renewal_date = labelToValue('Next Renewal Date', labelNodes);
  info.turn_on_cloud_licensing = labelToValue('Turn On Cloud Licensing', labelNodes);

  info.subscriptions = [];
  let rows = document.querySelectorAll('div.bRelatedList.first .customnotabBlock .dataRow');
  for (let i = 0; i < rows.length; i++) {
    let cells = rows[i].querySelectorAll('td.dataCell');
    info.subscriptions[i] = `${cells[0].textContent} : ${cells[3].textContent} * ${cells[4].textContent}`;
  }
  return pasteSfdcSubscription(info);
}

function pasteSfdcSubscription(info) {
  let text =
    `SFDC Subscription information:\n` +
    `URL = ${info.url}\n` +
    `Subscription Name = ${info.subscription_name}\n` +
    `Version = ${info.version}\n` +
    `Account = ${info.account}\n` +
    `Billing Account = ${info.billing_account}\n` +
    `Invoice Owner = ${info.invoice_owner}\n` +
    `Status = ${info.status}\n` +
    `Initial Term = ${info.initial_term}\n` +
    `Renewal Term = ${info.renewal_term}\n` +
    `Current Term = ${info.current_term}\n` +
    `Term Start Date = ${info.term_start_date}\n` +
    `Term End Date = ${info.term_end_date}\n` +
    `Subscription Start Date = ${info.subscription_start_date}\n` +
    `Subscription End Date = ${info.subscription_end_date}\n` +
    `Next Renewal Date = ${info.next_renewal_date}\n` +
    `Turn On Cloud Licensing = ${info.turn_on_cloud_licensing}\n`
  ;
  for (let i = 0; i < info.subscriptions.length; i++) {
    text += `${info.subscriptions[i]}\n`
  }
  return text;
}
