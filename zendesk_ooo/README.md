# Zendesk OOO

## Purpose

A userscript for Zendesk views.

This script changes the `Assignee OOO` title in the table header to just `OOO` so the table is a bit more compact.\
It also removes the text from the column, and shows a symbol when a person is OOO.

<img src="img/screenshot.png" height=400 />

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_ooo/script.user.js

## Warning

The script uses a very blunt approach to update the cell content. \
It just matches on the exact texts `Yes` and `No`.
Let me know if you find a way to target only the correct column!

Due to how Zendesk updates the tables, this script is not perfect. \
When you switch between Zendesk views you'll see that an incorrect column will have the `OOO` title.
If this bothers you, a page refresh will solve it.

Despite this deficiency I find the script very useful. 

## Technical

## Changelog

- 1.1
  - Remove cell text, show symbol for OOO
- 1.0
  - Public release
