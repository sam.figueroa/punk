// ==UserScript==
// @name          GitLab Todo
// @version       1.0
// @author        Rene Verschoor
// @description   GitLab: mark todos
// @match         https://gitlab.com/dashboard/todos
// @require       https://cdn.jsdelivr.net/npm/@violentmonkey/shortcut@1
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/gitlab_todo
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/gitlab_todo/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/gitlab_todo/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/gitlab_todo/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

(function() {

  const HOTKEY_CLOSED = buildHotkey('ctrl', 'tc');
  const HOTKEY_MERGED = buildHotkey('ctrl', 'tm');
  const HOTKEY_ALL = buildHotkey('ctrl', 'ta');

  function buildHotkey(modifier, chars) {
    let hotkey = '';
    for (let char of chars) {
      hotkey += `${modifier}-${char} `;
    }
    return hotkey.trim();
  }

  function registerHotkey() {
    VM.shortcut.register(HOTKEY_CLOSED, () => {
      toggle('Closed');
    });
    VM.shortcut.register(HOTKEY_MERGED, () => {
      toggle('Merged');
    });
    VM.shortcut.register(HOTKEY_ALL, () => {
      toggle('All');
    });
  }

  function toggle(target) {
    const todos = document.querySelectorAll('li.todo');
    todos.forEach( todo => {
      if (target === 'All' || todo.querySelector('.todo-target-state')?.textContent === target) {
        if (todo.classList.contains('done-reversible')) {
          todo.querySelector('div.todo-actions a.gl-button[data-method=patch]').click();
        } else {
          todo.querySelector('div.todo-actions a.gl-button[data-method=delete]').click();
        }
      }
    })
  }

  registerHotkey();

})();
