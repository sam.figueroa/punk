// ==UserScript==
// @name          Zendesk duplicate internal request info to CDot
// @version       3.10
// @author        Rene Verschoor
// @description   Zendesk: duplicate internal request info to CDot
// @match         https://gitlab.zendesk.com/*
// @match         https://customers.gitlab.com/*
// @match         https://customers.staging.gitlab.com/*
// @match         https://gitlab.com/gitlab-com/support/support-ops/support-ops-project/-/issues/*
// @grant         GM_registerMenuCommand
// @grant         GM_setValue
// @grant         GM_getValue
// @grant         GM_deleteValue
// @grant         GM_setClipboard
// @require       https://cdn.jsdelivr.net/npm/@violentmonkey/shortcut@1
// @require       https://cdn.jsdelivr.net/npm/sweetalert2@11
// @require       message.js
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_dup_ir_to_cdot
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_dup_ir_to_cdot/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_dup_ir_to_cdot/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_dup_ir_to_cdot/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

// https://github.com/violentmonkey/vm-shortcut
const HOTKEY_COPY = 'ctrl-i ctrl-c';
const HOTKEY_PASTE = 'ctrl-i ctrl-v';

const AUTHOR = 'GitLab Support End User';

const DELAY = 200;

function delay(ms) {
  return new Promise(res => setTimeout(res, ms));
}

(function () {

  function registerHotkey() {
    VM.shortcut.register(HOTKEY_COPY, async () => {
      await infoCopy();
    });
    VM.shortcut.register(HOTKEY_PASTE, async () => {
      await infoPaste();
    });
  }

  function registerMenu() {
    GM_registerMenuCommand('IR info - copy', infoCopy);
    GM_registerMenuCommand('IR info - paste', infoPaste);
  }

  async function infoCopy() {
    await GM_deleteValue('Dup_Info');
    let info = {};
    info.validInfo = false;
    let url = document.URL;
    if (url.match(/^https:\/\/gitlab.zendesk.com\/agent\/tickets\/\d+/)) {
      info = scrapeInfoInternalRequest(url);
    } else if (url.match(/^https:\/\/gitlab.com\/gitlab-com\/support\/support-ops\/support-ops-project\/-\/issues\/\d+/)) {
      info = await scrapeInfoInternalIssue(url);
    } else {
      toast_error('Can only copy info from a Zendesk ticket or Support Ops issue');
    }
    if (info.validInfo) {
      await GM_setValue('Dup_Info', JSON.stringify(info));
      toast_ok('Info copied');
    }
  }

  async function scrapeInfoInternalIssue(url) {
    let info = {};
    const title = getIssueTitle();
    if (title.startsWith('GitLab Team Member License request')) {
      info = scrapeInfo_team_member_license(url);
      info.validInfo = await validateTeamMemberLicense(info);
    } else {
      toast_error('Issue type not recognized');
    }
    return info;
  }

  function getIssueTitle() {
    return document.querySelector('h1.title').textContent;
  }

  function scrapeInfo_team_member_license(url) {
    const list = getIssueList();
    let values = {};
    values.type = 'TML';
    values.name = list['Name'];
    values.email = list['Email'];
    values.username = list['GitLab username'];
    values.company = 'GitLab - Team Member License';
    values.userscount = list['Number of users'];
    values.prevusercount = list['Previous user count'];
    values.trueups = list['True-up count'];
    values.plan = list['License plan'];
    values.expiration = list['Expiration date'];
    values.trial = 'No';
    values.notes = url;
    return values;
  }

  async function validateTeamMemberLicense(values) {
    let valid = validateGitLabEmail(values.email);
    if (valid) {
      let { validDate, adjustDate } = await validateExpirationDate(values.expiration);
      values.expiration1y = adjustDate;
      if (adjustDate) {
        values.expiration = oneYearFromNow();
        validDate = true;
      }
      valid = validDate;
    }
    return valid;
  }

  async function validateExpirationDate(exp) {
    const validDate = oneYearOrLess(exp);
    let adjustDate = false;
    if (!validDate) {
      let choice = await toast_confirm_choice('Validity can be max 1 year', 'Adjust to 1y');
      // Confirmed = adjust to 1y, Dismissed = canceled
      adjustDate = choice.isConfirmed;
    }
    return { validDate, adjustDate };
  }

  function oneYearOrLess(end) {
    const DAY_UNIT_IN_MILLISECONDS = 24 * 3600 * 1000
    const diffInMilliseconds = new Date(end).getTime() - new Date().getTime()
    const diffInDays = diffInMilliseconds / DAY_UNIT_IN_MILLISECONDS
    return (diffInDays <= 366);
  }

  function oneYearFromNow() {
    let today = new Date();
    today.setFullYear(today.getFullYear() + 1);
    today = today.toISOString().slice(0, 10);
    return today;
  }

  function validateGitLabEmail(value) {
    const domain = value.split('@')[1];
    const valid = (domain.trim().toLowerCase() === 'gitlab.com');
    if (!valid) {
      toast_error('Email must be @gitlab.com');
    }
    return valid;
  }

  function getIssueList() {
    const information = getInformation();
    return informationToList(information);
  }

  function getInformation() {
    const header = [...document.querySelectorAll('h2')].filter(header => header.textContent.trim() === 'Information');
    if (!header?.length) {
      toast_error('Issue has no Information header');
      return null;
    }
    if (!(header[0].nextElementSibling.nodeName === 'UL')) {
      toast_error('Issue has no Information list');
      return null;
    }
    const list = [...header[0].nextElementSibling.children].map(item => item.textContent.trim());
    if (!list?.length) {
      toast_error('Issue has no Information list items');
      return null;
    }
    return list;
  }

  function informationToList(information) {
    let list = {};
    information.forEach(text => {
      const keyval = text.split(/:(.*)/s);
      list[keyval[0].trim()] = keyval[1].trim();
    });
    return list;
  }

  function scrapeInfoInternalRequest(url) {
    let info = {};
    const title = getTicketTitle();
    if (title === 'Internal Request - SM - Extend an existing trial' || title === 'IR - SM - Extend an existing trial') {
      info = scrapeInfo_sm_extend_existing_trial(url);
    } else if (title === 'Internal Request - SM - Extend an (almost) expired subscription' || title === 'IR - SM - Extend an (almost) expired subscription') {
      info = scrapeInfo_sm_extend_expired_subscription(url);
    } else if (title === 'Internal Request - SM - Wider Community License' || title === 'IR - SM - Wider Community License') {
      info = scrapeInfo_sm_wider_community_license(url);
    } else if (title === 'Internal Request - SM - NFR license request' || title === 'IR - SM - NFR license request') {
      info = scrapeInfo_sm_nfr_license_request(url);
    } else if (title === 'Internal Request - SM - Hacker One Reporter License' || title === 'IR - SM - Hacker One Reporter License') {
      info = scrapeInfo_sm_hacker_one_license(url);
    } else if (title === 'Internal Request - SM - Problems starting a new trial' || title === 'IR - SM - Problems starting a new trial') {
      info = scrapeInfo_sm_start_new_trial(url);
    } else {
      toast_error('Ticket type not recognized');
    }
    return info;
  }

  function getTicketTitle() {
    return document.querySelector('div[role=tab][data-selected=true] [data-test-id="header-tab-title"]').textContent;
  }

  function getFirstComment() {
    let ticket= getActiveTicket();
    const comments = ticket.querySelectorAll('[data-test-id="omni-log-comment-item"]');
    if (!comments?.length) {
      toast_error('Ticket has no comments');
      return null;
    }
    const comment = comments[0];
    const author = comment.querySelector('[data-test-id=omni-log-item-sender]').textContent.trim();
    if (author !== AUTHOR) {
      toast_error(`Unexpected author: ${author}`);
      return null;
    }
    return comment;
  }

  function getActiveTicket() {
    let workspaces = document.querySelectorAll('.workspace');
    let activeWorkspace;
    for (let workspace of workspaces) {
      if (workspace.style['visibility'] !== 'hidden' && workspace.style['display'] !== 'none') {
        activeWorkspace = workspace;
        break;
      }
    }
    return activeWorkspace;
  }

  function commentToList(comment) {
    let list = {};
    const nodes = comment.querySelectorAll('.zd-comment ul li');
    const texts = [...nodes].map(node => node.textContent);
    texts.forEach(text => {
      const keyval = text.split(/:(.*)/s);
      list[keyval[0].trim()] = keyval[1].trim();
    });
    return list;
  }

  function getTicketList() {
    const comment = getFirstComment();
    return commentToList(comment);
  }

  function scrapeInfo_sm_extend_existing_trial(url) {
    const list = getTicketList();
    let values = scrapeTicket(list,
      "Contact's name",
      "Contact's email",
      "Company",
      "User count",
      null,
      "Plan",
      "Desired expiration date");
    values.trial = 'Yes';
    values.notes = url;
    values.type = 'IR';
    values.validInfo = true;
    return values;
  }

  function scrapeInfo_sm_extend_expired_subscription(url) {
    const list = getTicketList();
    let values = scrapeTicket(list,
      "Contact's name",
      "Contact's email",
      "Contact's company",
      "Seats needed in temp license",
      "True-ups needed in temp license",
      "Plan for temp license",
      "Desired expiration date");
    values.trial = 'Yes';
    values.notes = url;
    values.type = 'IR';
    values.validInfo = true;
    return values;
  }

  function scrapeInfo_sm_wider_community_license(url) {
    const list = getTicketList();
    let values = scrapeTicket(list,
      "Contact's name",
      "Contact's email",
      "Contact's company",
      "User count",
      null,
      "Plan",
      "Expiration date");
    values.trial = 'Yes';
    values.notes = `${url}, Wider Community`;
    values.type = 'IR';
    values.validInfo = true;
    return values;
  }

  function scrapeInfo_sm_nfr_license_request(url) {
    const list = getTicketList();
    let values = scrapeTicket(list,
      "Contact's name",
      "Contact's email",
      "Contact's company",
      "User count",
      null,
      "Plan",
      "License end date");
    values.trial = 'Yes';
    values.notes = `${url}, NFR`;
    values.type = 'IR';
    values.validInfo = true;
    return values;
  }

  function scrapeInfo_sm_hacker_one_license(url) {
    const list = getTicketList();
    let values = scrapeTicket(list,
      "Contact's name",
      "Contact's email",
      "Company",
      "Number of users",
      null,
      "Plan",
      "License duration");
    values.expiration = '';  // clear field again
    values.trial = 'Yes';
    values.notes = `${url}, HackerOne`;
    values.type = 'IR';
    values.validInfo = true;
    return values;
  }

  function scrapeInfo_sm_start_new_trial(url) {
    const list = getTicketList();
    let values = scrapeTicket(list,
      "Contact's name",
      "Contact's email",
      "Company",
      "Trial seat count",
      null,
      "Trial plan",
      "Desired trial end date");
    values.trial = 'Yes';
    values.notes = url;
    values.type = 'IR';
    values.validInfo = true;
    return values;
  }

  function scrapeTicket(list, name, email, company, userscount, trueups, plan, expiration) {
    let values = {};
    values.name = list[name];
    values.email = getEmail(list[email]);
    values.company = list[company];
    values.userscount = list[userscount];
    values.trueups = trueups ? list[trueups] : null;
    values.plan = list[plan];
    values.expiration = list[expiration];
    return values;
  }

  function getEmail(value) {
    // Contact's email: foo.bar@example.com (Zuora Search) (Portal Search)
    let [email, ...rest] = value.split(' ');
    return email;
  }

  async function infoPaste() {
    const url = document.URL;
    const info = JSON.parse(await GM_getValue('Dup_Info', '{}'));
    if (info.type === 'IR') {
      await infoPasteIR(url, info);
    } else if (info.type === 'TML') {
      await infoPasteTML(url, info);
      commentTML(info);
    } else {
      toast_error('Can only duplicate to CustomersDot > New License');
    }
  }

  async function infoPasteIR(url, info) {
    if (isCDotProduction(url) || isCDotStaging(url)) {
      await infoFill(info);
    } else {
      toast_error('Can only duplicate to CustomersDot > New License');
    }
  }

  async function infoPasteTML(url, info) {
    if (isCDotStaging(url)) {
      await infoFill(info);
    } else {
      toast_error('Can only duplicate to Staging CustomersDot > New License');
    }
  }

  function isCDotProduction(url) {
    return url.match(/^https:\/\/customers\.gitlab\.com\/admin\/license\/new_license/)
  }

  function isCDotStaging(url) {
    return url.match(/^https:\/\/customers\.staging\.gitlab\.com\/admin\/license\/new_license/)
  }

  function commentTML(info) {
    let comment = `Hi ${info.username}, \n`;
    if (info.expiration1y) {
      comment += 'Note that the license can only exist for one year at a time, so the expiration date has been adjusted. \\';
    }
    comment += `
The staging license has been generated at this time. \\
Search for the subject \`Your GitLab License File\` in your e-mail to locate it.

For information on applying a license, please read through [our documentation](https://docs.gitlab.com/ee/administration/license_file.html).

As this is a staging license, please keep in mind you *must* setup your GitLab deployment to work with those.

- For non-GDK instances, please see [our documentation](https://docs.gitlab.com/omnibus/development/setup.html#use-customers-portal-staging-in-gitlab).
- For GDK instances:
  - Ensure that you have a \`env.runit\` file created in the root directory of your GDK repository.
  - It should contain the following contents:

\`\`\`bash
export GITLAB_LICENSE_MODE=test
export CUSTOMER_PORTAL_URL="https://customers.staging.gitlab.com"
\`\`\`

- After the \`env.runit\` file is setup, please run \`gdk restart\`
- You should then be able to apply the license to your GDK instance

If you get an error that your license key is invalid, this will indicate you have not done the needed setup to work with a staging license.
/spent 2m
/label ~Readiness::Completed`;
    GM_setClipboard(comment, 'text/plain');
  }

  async function infoFill(info) {
    setField('license_name', info.name);
    setField('license_company', info.company);
    setField('license_email', info.email);
    setField('license_users_count', info.userscount);
    setCheckbox('license_trial', info.trial);
    setExpiresDate(info.expiration);
    setField('license_notes', info.notes);
    if (info.type === 'IR') {
      setTrueupsWarn(info.trueups);
    } else if (info.type === 'TML') {
      setTrueups(info.trueups);
      setField('license_previous_users_count', info.prevusercount);
    }
    await setLicenseType('Legacy License');
    await setPlan('license_plan_code', capitalize(info.plan));
  }

  function setField(field, value) {
    document.getElementById(field).value = value;
  }

  async function setLicenseType(type) {
    document.querySelector('[data-input-for="license_license_type"] .input-group-btn').click();
    await delay(DELAY);
    [...document.querySelectorAll('li')].filter(option => option.textContent === type)[0].click();
    await delay(DELAY);
  }

  function setTrueupsWarn(value) {
    if (value > 0) {
      toast_confirm("The request includes true-ups! You'll have to fill in PUC yourself!")
      setField('license_trueup_quantity', value);
    }
  }

  function setTrueups(value) {
    setField('license_trueup_quantity', value);
  }

  async function setPlan(field, plan) {
    document.querySelector('[data-input-for="license_plan_code"] .input-group-btn').click();
    await delay(DELAY);
    // bluntly scan all li items, dunno how to scan only the Plan list...
    [...document.querySelectorAll('li.ui-menu-item')].filter(listItem => listItem.textContent === plan)[0].click();
    await delay(DELAY);
  }

  function setExpiresDate(date) {
    let calendar = document.querySelectorAll('#license_expires_at_field div div .input-group input');
    calendar[1].focus();
    calendar[1].value = date;
    calendar[1].blur();
    document.getElementById('license_notes').focus();
  }

  function setCheckbox(field, text) {
    let value = text === 'Yes';
    let checkbox = document.getElementById(field);
    if (checkbox.checked !== value) {
      checkbox.click();
    }
  }

  function capitalize(text) {
    return text.charAt(0).toUpperCase() + text.slice(1);
  }

  registerMenu();
  registerHotkey();

})();

