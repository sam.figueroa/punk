// ==UserScript==
// @name          Zendesk few
// @version       1.4
// @author        Rene Verschoor
// @description   Zendesk: view fewer views
// @match         https://gitlab.zendesk.com/*
// @require       https://openuserjs.org/src/libs/sizzle/GM_config.js
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_few
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_few/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_few/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_few/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

const HEADERS_SELECTOR = '[data-test-id="views_views-list_accordion_header"]';
const VIEWS_SELECTOR = '[data-test-id="views_views-list_row"]';
const TITLE_SELECTOR = '[data-test-id="views_views-list_row_title"]';
const TOGGLE_PARENT = '[data-test-id="views_views-list_header-refresh"]';
const BUTTON_SVG = '<svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 24 24">' +
  '<path fill="none" stroke="currentColor" d="M12 24a12 12 0 1 1 12-12 12.013 12.013 0 0 1-12 12Zm0-22a10 10 0 1 0 10 10A10.011 10.011 0 0 0 12 2Z"/>' +
  '<path fill="none" stroke="currentColor" d="M10 7h8v2h-8zM6 7h2v2H6zM10 11h8v2h-8zM6 11h2v2H6zM10 15h8v2h-8zM6 15h2v2H6z"/>' +
  '</svg>';
let toggleAdded = false;

const iframecss = `
  height: 440px;
  width: 350px;
  border: 1px solid;
  border-radius: 3px;
  position: fixed;
  z-index: 9999;
`;

const css = `
  #FewConfig { 
    background: azure; 
  }
  .config_var {
    padding-bottom: 10px;
  }
  textarea {
    width: 330px;
    height: 200px;
  }
  #FewConfig_buttons_holder {
    float: right;
    width: 50%;
  }
  #FewConfig .reset_holder {
    float: right;
    position: relative;
    padding-right: 10px;
  }
`;

(function () {
  let gmc = defineConfig();
  let show_headers = null;
  let views_show = null;
  let views_list = null;

  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  const observer = new MutationObserver(mutations => {
    pogo(mutations);
  });
  observer.observe(document.body, {childList: true, subtree: true});

  function pogo(mutations) {
    mutations.forEach(mutation => {
      addToggle();
      hideHeaders(mutation);
      hideViews(mutation);
    })
  }

  function hideHeaders(mutation) {
    if (!show_headers) {
      mutation.target.querySelectorAll(HEADERS_SELECTOR).forEach(header => {
        header.remove();
      })
    }
  }

  function hideViews(mutation) {
    if (views_show === 'All views') return;
    mutation.target.querySelectorAll(VIEWS_SELECTOR).forEach(view => {
      const title = view.querySelector(TITLE_SELECTOR).innerText;
      if (views_list.contains(title)) {
        if (views_show === 'Hide list') {
          view.remove();
        }
      } else if (views_show === 'Show list') {
        view.remove();
      }
    })
  }

  function addToggle() {
    if (!toggleAdded) {
      const target = document.querySelector(TOGGLE_PARENT);
      if (target != null) {
        let toggle = document.createElement('button');
        toggle.innerHTML = BUTTON_SVG;
        target.parentNode.insertBefore(toggle, target);
        toggle.style.backgroundColor = 'rgb(0,0,0,0)';
        toggle.addEventListener('click', toggleClicked);
        toggleAdded = true;
      }
    }
  }

  function toggleClicked() {
    gmc.open();
    gmc.frame.style = iframecss;
  }

  function onConfigInit() {
    show_headers = gmc.get('headers');
    views_show = gmc.get('show');
    views_list = gmc.get('views_list').split('\n').filter(function(entry) { return entry.trim() !== ''; });
  }

  function onSave() {
    location.reload();
  }

  function defineConfig() {
    return new GM_config(
      {
        'id': 'FewConfig',
        'title': 'Few Settings',
        'fields': {
          'headers':
            {
              'label': 'Show section headers?',
              'type': 'checkbox',
              'default': true
            },
          'show':
            {
              'label': 'Which views to show?',
              'type': 'radio',
              'options': ['All views', 'Show list', 'Hide list'],
              'default': 'All views'
            },
          'views_list':
            {
              'label': 'Views',
              'type': 'textarea',
              'default': `My Assigned Tickets
Support Operations
SGG: Unsorted SaaS
SGG: Unsorted SM
SGG: Baobab
SGG: Ginkgo
SGG: Kapok
SGG: Maple
SGG: Pine
Assigned Support Engineer Tickets
L&R
GitLab Incidents
Secure
Authentication/Authorization
All FRT and Emergencies
All NRT
Escalated/feedback tickets
China Comms
Professional Services - Triage
Professional Services - Paid
Professional Services - Free
Billing
Accounts Receivables
Partner Support
New
Open
Pending
On-Hold
Solved
Closed
Global FRTs`
            },
        },
        'events': {
          'init': onConfigInit,
          'save': onSave
        },
        'css': css
      });
  }

})();
