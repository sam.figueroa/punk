# Zendesk few

## Purpose

Use Few to View Fewer Views

Here's my standard Zendesk view list.\
Depending on my window size and zoom level, some Personal views might drop off the window.\
I must make a conscious effort to regularly check the numbers shown for the views that I'm interested in and responsible for.

<img src="img/before.png" height=400 />

This is the result after configuring Few.\
Only the views relevant to me are shown.

<img src="img/after.png" height=400 />

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_few/script.user.js

## Configuration

When the userscript is loaded, you'll see a new icon at the top of the Views list:

<img src="img/toggle.png" height=300 />

If you click this icon, you'll get a configuration window.

### Show section headers?

Determines if the `Shared` and `Personal` header text is shown or hidden.

### Which views to show?

- All views: Show all views.
- Show list: Show the views that are listed in the `Views` list.
- Hide list: Hide the views that are listed in the `Views` list.

### Views

The list of views that need to be shown or hidden, depending on the `Which views to show?` setting.\
Specify one view name per line.

### Save

Save the configuration.\
The configuration window will close, and the browser page is refreshed to have the changes take effect.

### Close

Close the configuration window.\
If you made changes but didn't Save them, they will be lost.

### Reset to defaults

Reset settings to defaults.\
Useful to re-populate the `Views` list with a bunch of names, which saves some typing.\
Note that reset changes are not automatically saved.\
You can make modifications and then Save them, or discard the reset with Close.

## Changelog

1.4 - Config window is no longer butt-ugly
      You'll have to re-configure the views list, sorry
1.3 - Resized config window
1.2 - Automatic page refresh after Save
1.1 - Populate Views list
1.0 - First public release