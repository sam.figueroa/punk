# Zendesk trim org note

## Purpose

A userscript for Zendesk tickets.

This script removes entries from org notes that are duplicated from the sidebar.
This compacts the ticket, making it a bit more readable, and have you scroll less.

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_trim_org_note/script.user.js

## Technical

Essentially, it checks each `li`st and `h`eading item against a list of `const`ants,
and removes matching items.

## Changelog

- 1.5.0: Trim `NOTE`s about `Contact Management`/`Collaboration Project`
- 1.4.0: Trim subscription info
- 1.3.0: Trim Salesforce link & Solutions Architect
- 1.2.0: Trim headings
- 1.1.0: Update for Agent Workspace
- 1.0.0: Public release
