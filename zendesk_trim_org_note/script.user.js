// ==UserScript==
// @name          ZenDesk trim org notes
// @version       1.4.0
// @author        Katrin Leinweber
// @description   ZenDesk: Hide list items in org notes that are listed in sidebar already
// @match         https://gitlab.zendesk.com/*
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_trim_org_note
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_trim_org_note/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_trim_org_note/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_trim_org_note/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/-/issues/new
// ==/UserScript==

'use strict';

const DEBUG = false;
const ON  = "Organization Notes";
const OI  = "Organization Info";
const AT  = "Account type:";
const SS  = "Sales Segmentation:";
const AO  = "Account Owner:";
const CSM = "Customer Success Manager:";
const ENR = "Expiration/Next Renewal Date:";
const SFL = "Salesforce link:";
const SA = "Solutions Architect:";
const ADD = "This organization has the following active addons";
const NOP = "NOTE: This organization has a";
const SUB = "This organization has the following active subscription";
const SUB1 = "Self-Managed";
const SUB2 = "GitLab.com";

(function () {

  // Watch for any change on the Zendesk page
  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  const observer = new MutationObserver(function (mutations, observer) {
    po();
  });
  observer.observe(document.body, { childList: true, subtree: true });

  // Find note content
  function po() {
    let comments = document.querySelectorAll('[data-test-id="omni-log-comment-item"]');
    comments.forEach((comment, index) => {
      if (comment.querySelector('[data-test-id="omni-log-item-sender"]').textContent.trim() === 'GitLab SupportOps Bot') {

        let headers = comment.querySelectorAll('[data-test-id="omni-log-item-message"] .zd-comment h2');
        headers.forEach((header, index) => {
          let text = header.textContent
          if (text.startsWith(ON) ||
              text.startsWith(OI) ||
              text.startsWith(ADD)||
              text.startsWith(NOP)||
              text.startsWith(SUB)) {
            go(header);
          }
        })

        let contents = comment.querySelectorAll('[data-test-id="omni-log-item-message"] .zd-comment li');
        contents.forEach((content, index) => {
          let text = content.textContent
          if (text.startsWith(AT) ||
              text.startsWith(SS) ||
              text.startsWith(AO) ||
              text.startsWith(CSM)||
              text.startsWith(ENR)||
              text.startsWith(SFL)||
              text.startsWith(SA) ||
              text.startsWith(SUB1) ||
              text.startsWith(SUB2)) {
            go(content);
          }
        })
      }
    })
  }


  // Remove elements
  function go(element) {
    if (DEBUG) {
      element.style.backgroundColor = 'hotpink';
    } else {
      element.style.display = 'none';
    }
  }

})();
