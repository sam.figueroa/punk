// ==UserScript==
// @name          Zendesk Align Status & SLA
// @version       1.0
// @author        Rene Verschoor
// @description   Zendesk: Align Status & SLA
// @match         https://gitlab.zendesk.com/*
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_align_status_sla
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_align_status_sla/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_align_status_sla/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_align_status_sla/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

(function() {
  const observeNode = document.querySelector('body');
  MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
  const observer = new MutationObserver( mutations => {
    po(mutations);
  });
  observer.observe(observeNode, { childList: true, subtree: true });

  function po(mutations) {
    const selector_status = '[data-test-id="ticket-table-cells-status"]';
    const selector_sla = '[data-test-id="ticket-table-cells-sla"]';
    mutations.forEach( mutation => {
      mutation.target.querySelectorAll(selector_status).forEach( element => {
        element.style.verticalAlign = "middle";
        element.style.paddingTop = "0px";
      });
      mutation.target.querySelectorAll(selector_sla).forEach( element => {
        element.style.verticalAlign = "middle";
      });
    })
  }

})();
