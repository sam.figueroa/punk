# Zendesk SLA colors

## Purpose

A userscript for Zendesk tickets.

Zendesk uses two colors for SLA: green and red. \
This script improves that.

| Booooring... | Ohhhh, colors! |
| - | - |
| ![](img/zd.png) | ![](img/me.png) |

## Installation

Load the script in your userscript manager using the direct URL:

https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_sla_colors/script.user.js

## Changelog

- 1.0.4
  - Fixed selector, styling is now better/faster
- 1.0.3
  - Zendesk broke things again
  - Adding insult to injury, Zendesk switched to a spineless shade of green
- 1.0.2
  - Better update timing
  - Color scale rewrite
- 1.0.1
  - Fixed @match
- 1.0
  - Proof of Concept
