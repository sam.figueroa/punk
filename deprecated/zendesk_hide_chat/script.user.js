// ==UserScript==
// @name          Zendesk hide chat
// @version       1.0
// @author        Rene Verschoor
// @description   Zendesk: hide chat icon
// @match         https://gitlab.zendesk.com/*
// @grant         GM_addStyle
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_hide_chat
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_chat/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_chat/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_hide_chat/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

(function() {
  GM_addStyle('.user_option .apps_group { display: none }');
})();
