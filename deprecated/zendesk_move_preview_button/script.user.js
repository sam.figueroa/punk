// ==UserScript==
// @name          Zendesk move Preview button
// @version       1.0
// @author        Rene Verschoor
// @description   Zendesk: move Preview button to the left
// @match         https://gitlab.zendesk.com/*
// @grant         GM_addStyle
// @license       MIT
// @namespace     https://gitlab.com/rverschoor/punk
// @homepageURL   https://gitlab.com/rverschoor/punk/-/tree/main/zendesk_move_preview_button
// @downloadURL   https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_move_preview_button/script.user.js
// @installURL    https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_move_preview_button/script.user.js
// @updateURL     https://gitlab.com/rverschoor/punk/-/raw/main/zendesk_move_preview_button/script.user.js
// @supportURL    https://gitlab.com/rverschoor/punk/issues/new
// ==/UserScript==

'use strict';

(async function() {
  GM_addStyle(`.markdown_preview_toggle { float: none !important; }`);
})();

(function() {
})();
